# CRITERIA FOR ADDING A SERVER

Our [servers list](https://fediverse.party/en/portal/servers) promotes new small Fediverse instances, and we have the following criteria for inclusion:

1. At the moment we accept servers running following software: Mastodon (Glitch-Soc, Hometown), Pleroma (Akkoma), Friendica, Misskey (Firefish), Hubzilla (the list may be expanded in the future).

2. Only servers with open or pre-moderated registration are accepted. Servers with pre-moderated signup (a question like "Why do you want to join?" on signup) must be added to the category "Various (registration by application)".

3. Total number of users must not be more than 5000 (this number may be changed in the future).

4. Experimental new servers should not be put on the list. If you're unsure about the nearest future of your node, it's unwise to recommend it to newcomers.

5. If your server is proxied through Cloudflare, add note \*(Cloudflared)\* at the end. We may list only servers with non-strict Cloudflare settings that allow our automated checker to occasionally re-visit your server without tripping on Cloudflare's captchas. As soon as a server starts using strict settings and checking fails on our side, the server will be removed from the list.

Add your own themed instance via pull request or send a suggestion to [@lostinlight](https://mastodon.xyz/@lightone) via Fediverse. Thank you!
