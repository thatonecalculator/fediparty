
# GUIDELINES

Thanks for considering to contribute to Fediverse.Party.

Please note that we have some basic rules:

1. All the software listed on this website *must* be fully open source.

2. Any ideas for merge requests that suggest major changes *should* be discussed first. If in doubt, open an issue and list all the changes you want to submit. This will make sure that our expectations and visions are aligned.

3. Technology may be neutral, but @lostinlight, the creator of this website, has some subjective views. As of this writing, she is cautiously skeptical about widespread adoption of cryptocurrencies and blockchain. For this reason, lostinlight decided not to list software with cryptocurrency integrations, or networks based on blockchain, effective August 27th 2022. There is a separate [Wiki page](https://codeberg.org/fediverse/fediparty/wiki/blockchain-social-apps) for such projects, to be aware of new developments in these areas.

Thanks for considering to write for Fediverse.Party.

Contact [@lostinlight](https://mastodon.xyz/@lightone), [@smallcircles](https://social.coop/@humanetech) or [@minoru](https://functional.cafe/@minoru) via a Direct Message in case of any questions or if you need help with brainstorming ideas for an article, proofreading, conducting an interview.

Here are some guidelines that may help:

0. Fediverse.party covers all software federating via one of the four protocols: OStatus, diaspora, Zot, ActivityPub - your article will most likely be about one or several of such projects.

1. If you have already published something interesting about federated networks on your blog or personal website, the link to your external article may be added to the relevant section of this website. We'd like to post unique content as internal articles rather than mirror content already published somewhere else.

2. This website's target audience is newcomers to Fediverse. They may or may not have any technical background. Don't hesitate to explain simple "evident" concepts - it's better than assuming everyone will understand.

3. Articles are fun with illustrations. A banner illustration is required for the header, but may be you'll think of more images to make your narration visually appealing. If you need help with finding suitable images, contact [@lostinlight](https://mastodon.xyz/@lostinlight).

4. We all believe in something and support someone, we all differ in our preferences. Try to avoid religious and political bashing where possible. Please, be inclusive.

5. Laughter is the best medicine! Jokes highly appreciated in any narration, as long as they respect all human beings and aren't too niche (we'd like readers to understand them:).

Read [Contributing](./CONTRIBUTING.md) for technical details.
