---
layout: "post"
title: "Themed servers"
---

**Unsure where to register to join Fediverse?** Choose a website from this curated list. It includes [Mastodon](/en/mastodon), Glitch-soc and Hometown (Mastodon forks), [Pleroma](/en/pleroma) and Akkoma (Pleroma fork), [Friendica](/en/friendica), [Misskey](/en/misskey) and Firefish (Misskey fork), and [Hubzilla](/en/hubzilla) servers. Websites are not restricted to their theme. They help people with common interests find their community.

Information about adding a server can be found [here](https://codeberg.org/fediverse/fediparty/src/branch/main/ADDING-SERVER.md).

<ul class="article-list">

### 💡 [Sciences](#sciences)
* [lugnasad.eu](https://lugnasad.eu) - for French speaking science lovers *(Friendica)*
* [humangenetics.social](https://humangenetics.social) - for people interested in the science of human genetics *(Mastodon)*
* [science.social](https://science.social) - for scientists *(Mastodon)*

### 🎨 [Humanities](#humanities)
* [oulipo.social](https://oulipo.social) - a lipogrammatic server *(Mastodon)*
* [archaeo.social](https://archaeo.social) - for archaeologists, historians and lovers of all things ancient *(Mastodon)*
* [zirk.us](https://zirk.us) - for readers, writers, thinkers, artists, academics, enthusiasts in arts and humanities *(Mastodon)*

### 🎓 [Education](#education)
* [tusk.schoollibraries.net](https://tusk.schoollibraries.net) - helping educators improve the School Libraries Resource Network *(Mastodon)*
* [mastodon.oeru.org](https://mastodon.oeru.org) - for educators and learners involved in the [OERu](https://oeru.org) *(Mastodon)*
* [akademienl.social](https://akademienl.social) - for anyone working in or with a general interest in Dutch academia *(Mastodon)*
* [mastodon.education](https://mastodon.education) - for everyone who works for and with education *(Mastodon)*
* [sotl.social](https://sotl.social) - talk about the Scholarship of Teaching and Learning *(Mastodon)*

### 🎵 [Music](#music)
* [koreadon.com](https://koreadon.com) - for K-POP music fans *(Mastodon)*
* [feedbeat.me](https://feedbeat.me) - dedicated to culture and events such as music, poetry, comedy (German) *(Mastodon)*
* [piano.masto.host](https://piano.masto.host) - dedicated to piano music, in English/Japanese *(Mastodon)*
* [metalverse.social](https://metalverse.social) - from Metalheads for everybody *(Mastodon)*
* [drumstodon.net](https://drumstodon.net) - a place for drummers, musicians and music lovers in general *(Mastodon)*
* [musician.social](https://musician.social) - for musicians who create, play, or love jazz, rock, pop, indie, classical and all other types of music *(Mastodon)*
* [douzepoints.social](https://douzepoints.social) - for all Eurovision fans *(Mastodon)*
* [epsilon.social](https://epsilon.social) - for nerds who love electronic music, synth DIY and vinyl *(Mastodon)*.

### 🔭 [Interests and hobbies](#hobbies)
* [screenwriting.space](https://screenwriting.space) - a place for storytellers *(Mastodon)*
* [rollenspiel.social](https://rollenspiel.social) - (German) roleplay, Pen & Paper, tabletop, TCG, for all gamers *(Mastodon)*
* [radiosocial.de](https://radiosocial.de) - for German radio amateurs *(Mastodon)*
* [hamradio.tel](https://hamradio.tel) - ham radio community *(Mastodon)*
* [pl.nudie.social](https://pl.nudie.social) - for naturists, nudists, clothes-free living *(Akkoma)*
* [prf.me](https://prf.me) - for perfume, fragrance and scent lovers *(Mastodon)*
* [makerspace.social](https://makerspace.social) - space for makers (CNC, woodworking, microcontrollers, etc) *(Mastodon)*
* [3dp.chat](https://3dp.chat) - 3D printing *(Mastodon)*
* [bikesare.cool](https://bikesare.cool) - space for competitive and casual cyclists *(Mastodon)*
* [social.vtopia.live](https://social.vtopia.live) - for Vtubers, their fans, and the creators that make them possible *(Misskey)*
* [gametoots.de](https://gametoots.de) - for gamers and streamers *(Mastodon)*
* [hub.adrelien.com](https://hub.adrelien.com) - for all sports, e-sports, and gaming enthusiasts *(Firefish)* *(Google reCAPTCHA)*
* [mastodon.triggerphra.se](https://mastodon.triggerphra.se) - for the hypnosis community; 18+, queer, and getting very sleepy *(Mastodon)*
* [moth.orchids.social](https://moth.orchids.social) - for orchid lovers *(Mastodon)*

### 🚄  [Travel, Transport and Infrastructure](#travel)
* [rail.chat](https://rail.chat) - discussions about long-distance, passenger and freight rail networks for economic, environmental and equity benefits *(Mastodon)*
* [aircrew.rocks](https://aircrew.rocks) - for pilots, flight attendants, and flight enthusiasts *(Mastodon)*
* [bahn.social](https://bahn.social) - for rail enthusiasts, in German *(Mastodon)*
* [toot.pizza](https://toot.pizza) - NYC-leaning urbanism and transit instance *(Mastodon)*
* [towns.gay](https://towns.gay) - for radical queers, who believe that cities are for us too *(Mastodon)*
* [geobla.net](https://geobla.net) - for vanlife and camping *(Mastodon)*

### 🎏 [Language specific](#languages)
* [toki.social](https://toki.social) - for those interested in toki pona *(Mastodon)*
* [mastodon.sk](https://mastodon.sk) - for Slovak users *(Mastodon)*
* [mastodon.fedi.bzh](https://mastodon.fedi.bzh) - for Breton and Gallo speakers *(Mastodon)*
* [gomastodon.cz](https://gomastodon.cz) - for Czech users *(Mastodon)*
* [mk.phreedom.club](https://mk.phreedom.club) - for Russian users *(Misskey)*
* [librosphere.fr](https://librosphere.fr) - for French speakers *(Pleroma)*
* [norrebro.space](https://norrebro.space) - for Danish speakers *(Mastodon)*
* [loðfíll.is](https://xn--lofll-1sat.is) - for Icelandic speakers *(Mastodon)*
* [best-friends.chat](https://best-friends.chat) - for Japanese speakers *(Mastodon)*
* [mastodon.in.th](https://mastodon.in.th) - for Thai speakers *(Mastodon)*
* [occitania.social](https://occitania.social) - community of Occitan culture and language *(Mastodon)*
* [samenet.social](https://samenet.social) - Sámi language *(Mastodon)*
* [mastodo.fi](https://mastodo.fi) - Finnish language *(Mastodon)*
* [vkl.world](https://vkl.world) - Belarusian language *(Mastodon)*
* [9kb.me](https://9kb.me) - Chinese language *(Mastodon)*
* [fairy.id](https://fairy.id) - Chinese language *(Mastodon)*
* [e.fo](https://e.fo) - Faroese language *(Mastodon)*
* [kanoa.de](https://kanoa.de) - German language *(Mastodon)*
* [electricrequiem.com](https://electricrequiem.com) - Greek language *(Firefish)* *(Google reCAPTCHA)*

### 🛡 [Safer spaces](#safer-spaces)

Instances run by and for people belonging to minorities and moderated more strictly than usual
(also check out [Fedi.Garden](https://fedi.garden) for curated list of well-moderated instances)

* [neovibe.app](https://neovibe.app) - for fans of music, movies, gaming and all forms of entertainment, LGBTQ+ friendly and Black-run *(Mastodon)*
* [qdon.space](https://qdon.space) - a queer-friendly Korean-language instance - 퀴어 친화적인 마스토돈 인스턴스입니다 *(Mastodon)*
* [bark.lgbt](https://bark.lgbt) - for critters that like to bark, LGBTQ+ run and friendly, active moderation *(Mastodon)*

### ⛺ [Regional](#regional)

#### Australia
* [bne.social](https://bne.social) - Brisbane *(Mastodon)*

#### Austria
* [krems.social](https://krems.social) - Krems *(Mastodon)*
* [fedi.at](https://fedi.at) - Austria *(Mastodon)*
* [sbg-social.at](https://sbg-social.at) - Salzburg *(Mastodon)*
* [aut.social](https://aut.social) - Austria *(Mastodon)*

#### Belgium
* [wokka.be](https://wokka.be) - Belgium
* [mastodon-belgium.be](https://mastodon-belgium.be) - Belgium *(Mastodon)*

#### Canada
* [vancity.social](https://vancity.social) - Vancouver *(Firefish)*

#### Czech Republic
* [witter.cz](https://witter.cz) - Czech Republic *(Mastodon)*

#### Denmark
* [norrebro.space](https://norrebro.space) - Denmark *(Mastodon)*

#### Egypt
* [twitteregypt.online](https://twitteregypt.online) - Egypt *(Mastodon)*

#### Finland
* [mastodo.fi](https://mastodo.fi) - Finland *(Mastodon)*

#### Germany
* [mastodon.bayern](https://mastodon.bayern) - Bavaria *(Mastodon)*
* [ruhrpott.social](https://ruhrpott.social) - Ruhr area *(Mastodon)*
* [fulda.social](https://fulda.social) - Fulda *(Mastodon)*
* [brandenburg.social](https://brandenburg.social) - Brandenburg *(Mastodon)*
* [berlin.social](https://berlin.social) - Berlin *(Mastodon)*
* [osna.social](https://osna.social) - Osnabrück *(Mastodon)*
* [fem.social](https://fem.social) - Ilmenau *(Mastodon)*
* [friendica.a-zwenkau.de](https://friendica.a-zwenkau.de) - Zwenkau *(Friendica)*
* [harz.social](https://harz.social) - Harz area *(Mastodon)*
* [rheinneckar.social](https://rheinneckar.social) - Rhein-Neckar area *(Mastodon)*
* [moessingen.social](https://moessingen.social) - Mössingen area *(Mastodon)*
* [chemnitz.network](https://chemnitz.network) - Chemnitz *(Mastodon)*
* [chemnitz.rocks](https://chemnitz.rocks) - Chemnitz *(Friendica)*
* [mfr.social](https://mfr.social) - (Mittel-)Franken *(Mastodon)*
* [cas.social](https://cas.social) - Castrop-Rauxel *(Mastodon)*
* [im.allmendenetz.de](https://im.allmendenetz.de) - Cologne, Germany *(Hubzilla)*
* [toot.berlin](https://toot.berlin) - Berlin *(Mastodon)*
* [kowelenz.social](https://kowelenz.social) - Koblenz *(Akkoma)*

#### Indonesia
* [mstdn.id](https://mstdn.id) - Indonesia *(Mastodon)*
* [mastodonindonesia.com](https://mastodonindonesia.com) - Indonesia *(Mastodon)*

#### Italy
* [foxyhole.io](https://foxyhole.io) - Italy *(Firefish)* *(hCaptcha)*

#### Nigeria
* [mastodon.holeyfox.co](https://mastodon.holeyfox.co) - Nigeria *(Mastodon)*

#### Netherlands
* [nwb.social](https://nwb.social) - Nieuw West-Brabant *(Mastodon)*

#### Paraguay
* [mastodon.com.py](https://mastodon.com.py) - Paraguay *(Mastodon)*

#### Portugal
* [lisboa.social](https://lisboa.social) - Lisbon *(Mastodon)*

#### Spain
* [mstdn.es](https://mstdn.es) - Spain *(Mastodon)*
* [malaga.social](https://malaga.social) - Malaga *(Mastodon)*

#### Switzerland
* [swiss.social](https://swiss.social) - Switzerland *(Mastodon)*
* [mastodon.free-solutions.org](https://mastodon.free-solutions.org) - Switzerland *(Mastodon)*
* [swiss-talk.net](https://swiss-talk.net) - Switzerland *(Mastodon)*

#### Ukraine
* [soc.ua-fediland.de](https://soc.ua-fediland.de) - Ukraine *(Mastodon)*
* [lviv.social](https://lviv.social) - Lviv *(Mastodon)*

#### UK
* [bath.social](https://bath.social) - Bath *(Mastodon)*

#### USA
* [socialclub.nyc](https://socialclub.nyc) - New York *(Mastodon)*
* [social.tulsa.ok.us](https://social.tulsa.ok.us) - Tulsa, Northeast Oklahoma *(Mastodon)*
* [cityofchicago.live](https://cityofchicago.live) - Chicago, Illinois *(Mastodon)*
* [gardenstate.social](https://gardenstate.social) - New Jersey *(Mastodon)*

### 🐧 [For techies](#servers-for-techies)
* [techlover.eu](https://techlover.eu) - talk about new technologies like development, digital art or science *(Mastodon)*
* [layer8.space](https://layer8.space) - talk about Linux, anime, music, software and more *(Mastodon)*
* [devschile.social](https://devschile.social) - for Spanish-language techies of Chile *(Mastodon)*
* [k8s.social](https://k8s.social) - for kubernetes, container and cloud native enthusiasts *(Mastodon)*
* [gnulinux.social](https://gnulinux.social) - community dedicated to Free Software enthusiasts and supporters
* [technodon.org](https://technodon.org) - general technology discussion *(Mastodon)* *(Cloudflared)*
* [toot.works](https://toot.works) - for folks who make things with bits, wires, wood, and more *(Mastodon)*

### 💻 [Programming](#instances-for-programmers)
* [pythondevs.social](https://pythondevs.social) - for Python developers of all experience levels *(Pleroma)*
* [dotnet.social](https://dotnet.social) - .NET *(Mastodon)*
* [jvm.social](https://jvm.social) - JVM technologies related server *(Mastodon)*

### 🎬 [Book / Game / Show theme](#entertainment)
* [greenhill.zone](https://greenhill.zone) - multilingual instance for fans of Sonic the Hedgehog *(Mastodon)*

### 🐰 [Environmentalism](#environmentalism)
* [toot.cat](https://toot.cat) - instance for cats, the people who love them, and kindness in general *(Mastodon)*
* [floe.earth](https://mastodon.floe.earth) - talk about climate, society, but also pleasant things in life *(Mastodon)*
* [sauropods.win](https://sauropods.win) - for sauropod appreciators *(Mastodon)*
* [ecocentraal.social](https://ecocentraal.social) - about eco-central world *(Mastodon)*
* [bhre.social](https://bhre.social) - talk about business, human rights and the environment (BHRE) *(Mastodon)*
* [vegoon.party](https://vegoon.party) -  focus on veganism *(Mastodon)*
* [veganism.social](https://veganism.social) - for vegans and animal rights activists *(Mastodon)*

### ⚕ [Healthcare](#health)
* [fedisabled.social](https://fedisabled.social) - for all disabled people *(Mastodon)*
* [medibubble.org](https://medibubble.org) - for medical professionals and everyone else, in German *(Mastodon)*

### 👽 [Fandoms](#fandoms)
* [bungle.online](https://bungle.online) - fandom instance, post media, fan art and fanfiction *(Misskey)*
* [mastodol.jp](https://mastodol.jp) - talk about idols in Japanese *(Mastodon)*

### 🐾 [Subcultures](#subcultures)
* [birds.garden](https://birds.garden) - a quiet, peaceful place where any bird can find refuge *(Pleroma)*
* [stop.voring.me](https://stop.voring.me) - fun hangout for furries and normies alike *(Firefish)* *(hCaptcha)*

### 🙏 [Religion](#religion)
* [elizur.me](https://elizur.me) - for German-speaking Christians *(Mastodon)*
* [babka.social](https://babka.social) - for Jews and Jewish allies *(Mastodon)*

### 🔴  [Political and social views](#political-and-social-views)
* [elonsucks.org](https://elonsucks.org) - English-speaking server open to anyone, especially those who are anti-capitalist, pro-democracy and strive to bridge inequality *(Mastodon)* *(Cloudflared)*

### 🌏 [Notable generalistic](#notable-generalistic)
// *small-to-medium sized instances that will be happy to have new users*
* [venera.social](https://venera.social) - friendly humans are welcome *(Friendica)*
* [misskey.de](https://misskey.de) - hosted in Helsinki *(Misskey)*
* [wienermobile.rentals](https://wienermobile.rentals) - for all reasonable people *(Pleroma)*
* [howlr.me](https://howlr.me) - for members of niche and alternative lifestyle communities; originally alterhuman focused *(Akkoma)*
* [social.sp-codes.de](https://social.sp-codes.de) - main language is German *(Mastodon)*
* [social.yesterweb.org](https://social.yesterweb.org) - hosted in the USA, mainly in English *(Mastodon)*
* [wetdry.world](https://wetdry.world) - talk about tech, gaming, bad jokes *(Mastodon)*
* [masto.bike](https://masto.bike) - riding a bike is a plus, French speaking *(Mastodon)*
* [blueplanet.social](https://blueplanet.social) - for people interested in making this planet a better place, primarily German language
* [convo.casa](https://convo.casa) - general for everyone *(Mastodon)*
* [publicsquare.global](https://publicsquare.global) - safe place in Fediverse dedicated to providing online communication as a basic human right *(Mastodon)* *(Cloudflared)*

### 🐚 [Run by tech-savvy organizations](#run-by-tech-savvy-organizations)
*Instances which declare to be affiliated with an incorporate non-profit (usually an [association](https://en.wikipedia.org/wiki/Voluntary_association) or [cooperative](https://www.ica.coop/en/cooperatives/what-is-a-cooperative)) and to follow its policies*
* [en.osm.town](https://en.osm.town) - for the OpenStreetMap Community *(Mastodon)*
* [swiss-chaos.social](https://swiss-chaos.social) - Chaos Computer Club Switzerland *(Mastodon)*

### 🎉 [Notable mention](#notable-mention)
// *are open to particular audience, i.e., to students of university*
* [mastodon.mit.edu](https://mastodon.mit.edu) - for the MIT community *(Mastodon)*
* [metu.life](https://metu.life) - for METU university of Turkey *(Mastodon)*
* [mastodon.librelabucm.org](https://mastodon.librelabucm.org) - for Universidad Complutense de Madrid (UCM) people and free/libre software talk in Spanish *(Mastodon)*
* [mastodon.acc.sunet.se](https://mastodon.acc.sunet.se) - by Academic Computer Club at Umeå University, Sweden *(Mastodon)*
* [akademienl.social](https://akademienl.social) - for anyone working in, affiliated with, or with a general interest in Dutch academia  *(Mastodon)*
* [social.sunet.se](https://social.sunet.se) - by the Swedish Research Council for anyone belonging to a Swedish university *(Mastodon)*
* [social.edu.nl](https://social.edu.nl) - by [SURF](https://www.wikidata.org/wiki/Q2422744), a cooperative of Dutch universities *(Mastodon)*
* [social.mpdl.mpg.de](https://social.mpdl.mpg.de) - by the [Max Planck Digital Library](https://www.mpdl.mpg.de/) at MPG *(Mastodon)*
* [wisskomm.social](https://wisskomm.social) - by [idw](https://de.wikipedia.org/wiki/Informationsdienst_Wissenschaft), for entities and groups in German academia *(Mastodon)*
* [social.up.edu.ph](https://social.up.edu.ph) - by the University of the Philippines *(Hometown)*

### 🎉 [Various (registration by application)](#registration-by-application)

#### Minorities (registration by application)
*Instances focused on providing a safe space for minorities such as LGBTQ+ people and require approval for registration*
* [4bear.com](https://4bear.com) - for the LGBT+ Bear Community *(Mastodon)*
* [indiepocalypse.social](https://indiepocalypse.social) - for independent creators of all sorts *(Mastodon)*
* [is.nota.live](https://is.nota.live) - queer-run instance *(Mastodon)*
* [lediver.se](https://lediver.se) - für Leipzig, Diversität - diversity-friendly German-language instance *(Mastodon)*
* [lgbtqia.space](https://lgbtqia.space) - for all LGBTQIA people who want a caring and safe environment *(Mastodon)*
* [meemu.org](https://meemu.org) - a queer-friendly instance *(Mastodon)*
* [outdoors.lgbt](https://outdoors.lgbt) - a safe space for LGBTQIA+ individuals and staunch allies that love the outdoors *(Mastodon)*
* [pipou.academy](https://pipou.academy) - a French-language queer instance - une instance queer, qui vise à être aussi confortable et safe que possible *(Mastodon)*
* [poweredbygay.social](https://poweredbygay.social) - for those LGBTQIA+ (and Allies) that are into fun adventurous things, like playing/watching sports, hiking, camping *(Mastodon)*
* [retro.pizza](https://retro.pizza) - a queer-run poly-fandom nerd culture instance *(Mastodon)*
* [connectop.us](https://connectop.us) - a respectful, diverse, inclusive community standing up for all marginalized gropus *(Mastodon)*
* [blackqueer.life](https://blackqueer.life) - for Black queer folks *(Mastodon glitch)*

#### Furry (registration by application)
*Instances which are focused on providing a safe space for furry interests and require approval for registration.*
* [chitter.xyz](https://chitter.xyz) - for a friendly, inclusive, and incredibly soft community *(Mastodon)*
* [furry.engineer](https://furry.engineer) - echies and engineers of all types within the furry fandom, LGBTQ+ friendly *(Mastodon)*
* [pawb.fun](https://pawb.fun) - for the furry fandom, LGBTQ+ friendly safe space *(Mastodon)*
* [pounced-on.me](https://pounced-on.me) - a generalistic furry mastodon instance *(Mastodon glitch)*
* [thicc.horse](https://thicc.horse) - for body positivity, LGBTQIA+, Furry, horny on main *(Mastodon)*
* [pony.social](https://pony.social) - for nerds, LGBTQ+ or just people looking for a comfy place *(Mastodon)*
* [derg.social](https://derg.social) - Swiss furry community *(Mastodon)* *(Cloudflared)*

#### Other
// *these servers may be removed in future automated updates due to signups by application*
* [augsburg.social](https://augsburg.social) - Augsburg, Germany *(Mastodon)*
* [wue.social](https://wue.social) - Würzburg (and the neighbourhood), Germany *(Mastodon)*
* [bonn.social](https://bonn.social) - Bonn, Germany *(Mastodon)*
* [krefeld.life](https://krefeld.life) - Krefeld, Germany *(Mastodon)*
* [darmstadt.social](https://darmstadt.social) - Darmstadt, Germany *(Mastodon)*
* [snabelen.no](https://snabelen.no) - Norway *(Mastodon)*
* [tukkers.online](https://tukkers.online) - Twente region, Netherlands *(Mastodon)*
* [mastodon.opencloud.lu](https://mastodon.opencloud.lu) - Luxembourg (private and public organisations) *(Mastodon)*
* [mastodon.uy](https://mastodon.uy) - Uruguay *(Mastodon)*
* [chilemasto.casa](https://chilemasto.casa) - Chile *(Mastodon)*
* [mastodos.com](https://mastodos.com) - Kyoto, Japan *(Mastodon)*
* [mograph.social](https://mograph.social) - for motion design community, VFX / 3D artists, animators, illustrators *(Mastodon)*
* [iztasocial.site](https://iztasocial.site) - discussions about psychology for professors of SUAyED Psicología  university of Mexico *(Mastodon)*
* [openbiblio.social](https://openbiblio.social) - German libraries and information facilities *(Mastodon)*
* [ausglam.space](https://ausglam.space) - Australian galleries, libraries, archives, museums and records people *(Mastodon)*
* [linernotes.club](https://linernotes.club) - for discussing music recordings *(Mastodon)*
* [bostonmusic.online](https://bostonmusic.online) - digital space serving the Boston music scene *(Mastodon)*
* [mastodon.radio](https://mastodon.radio) - for Amateur (Ham) Radio community *(Mastodon)*
* [mastodontti.fi](https://mastodontti.fi) - Finnish, public toots should be in Finnish *(Mastodon)*
* [floss.social](https://floss.social) - for people who support or build Free Libre Open Source Software *(Mastodon)*
* [linuxrocks.online](https://linuxrocks.online) - dedicated to Linux and technologies *(Mastodon)*
* [digipres.club](https://digipres.club) - conversations about digital preservation *(Mastodon)*
* [phpc.social](https://phpc.social) - PHP community
* [dads.cool](https://dads.cool) - anyone with a kid can be a dad *(Mastodon)*
* [colorid.es](https://colorid.es)- Spanish LGBTQIA+ / queer instance primarily for Portuguese speakers *(Mastodon)*
* [fandom.ink](https://fandom.ink) - for fans and fandoms of all types *(Mastodon)*
* [tooting.ch](https://tooting.ch) - generic instance hosted by the FairSocialNet association *(Mastodon)*
* [digitalcourage.social](https://digitalcourage.social) - by DigitalCourage *(Mastodon)*
* [libretooth.gr](https://libretooth.gr) - by LibreOps, who contribute to (re-)decentralizing the net *(Mastodon)*
* [mastodon.cc](https://mastodon.cc) - for art *(Mastodon)*
* [theres.life](https://theres.life) - for Christians *(Mastodon)*
* [fikaverse.club](https://fikaverse.club) - Swedish and other Scandinavian languages *(Mastodon)*
* [wpbuilds.social](https://wpbuilds.social) - for those who love Wordpress and open source *(Mastodon)*
* [feuerwehr.social](https://feuerwehr.social) - for all firefighters in German-speaking countries *(Mastodon)*
* [mastodon.gougere.fr](https://mastodon.gougere.fr) - Auxerre, France *(Mastodon)*
* [stereodon.social](https://stereodon.social) - self-managed social network devoted to underground music (Italian, English languages) *(Mastodon)*
* [solarpunk.moe](https://solarpunk.moe) - for solarpunk nerds *(Mastodon)*
* [leftist.network](https://leftist.network) - for those with Leftist politics *(Mastodon)*
* [PCGamer.social](https://pcgamer.social) - by the PC gamer, for the PC gamer *(Mastodon)*
* [poliverso.org](https://poliverso.org) - dedicated to politics and digital rights (Italian speaking) *(Friendica)*
* [ruby.social](https://ruby.social) - for people interested in Ruby, Rails and related topics *(Mastodon)*
* [activism.openworlds.info](https://activism.openworlds.info) - for activists *(Mastodon)*
* [mastodontti.fi](https://mastodontti.fi) - for Finnish speakers *(Mastodon)*
* [snabelen.no](https://snabelen.no) - for Norwegian speakers *(Mastodon)*
* [mastodon.ml](https://mastodon.ml) - Russia *(Mastodon)*
* [cmdr.social](https://cmdr.social) - dedicated to Elite: Dangerous and space exploration *(Mastodon)*
* [fsmi.social](https://fsmi.social) - free software movement of India *(Mastodon)*
* [handmade.social](https://handmade.social) - for all handmade artisans and their Etsy shops
* [sciencemastodon.com](https://sciencemastodon.com) -  for science journalists and scientists
* [recht.social](https://recht.social) - focus on legal topics *(Mastodon)*
* [medic.cafe](https://medic.cafe) - for employees in the medical field *(Mastodon)*
* [graz.social](https://graz.social) - Graz, Austria *(Mastodon)*
* [leafposter.club](https://leafposter.club) - Canada *(Pleroma)*
* [sciences.social](https://sciences.social) - for social scientists *(Mastodon)*
* [indiepocalypse.social](https://indiepocalypse.social) - for independent creators of all sorts *(Mastodon)*
* [brettspiel.space](https://brettspiel.space) - for boardgame players *(Mastodon)*
* [podcasts.social](https://podcasts.social) - for podcasters *(Mastodon)*
* [beo.social](https://beo.social) - Bernese Oberland, Switzerland
* [greennuclear.online](https://greennuclear.online) - nuclear community *(Mastodon)*
* [indiehackers.social](https://indiehackers.social) - for all indie hackers *(Mastodon)*
* [musicians.today](https://musicians.today) - for musicians of all levels, instruments, regions, languages, and genres *(Mastodon)*
* [kalmar.social](https://kalmar.social) - Kalmar region, Sweden *(Mastodon)*
* [blasmusik.social](https://blasmusik.social) - specially, but not only for brass musicians, Germany *(Mastodon)*
* [social.seattle.wa.us](https://social.seattle.wa.us) - Seattle *(Mastodon)*
* [social.bau-ha.us](https://social.bau-ha.us) - part of CCC family, hosted in Weimar *(Mastodon)*
* [poliversity.it](https://poliversity.it) - Italian instance dedicated to the world of science and journalism *(Mastodon)*
* [astrodon.social](https://astrodon.social) - for anyone interested in astronomy, astrophysics, astrophotography *(Mastodon)*
* [artisan.chat](https://artisan.chat) - for creators, crafters, and artists *(Mastodon)*
* [onlycosplays.social](https://onlycosplays.social) - safe place for cosplayers and their communities *(Mastodon)*
* [mastodon.eus](https://mastodon.eus) - for Euskera/Basque speakers *(Mastodon)*
* [xarxa.cloud](https://xarxa.cloud) - for Catalan and Spanish speakers *(Mastodon)*
* [myhobby.zone](https://myhobby.zone) - for all kinds of hobbies *(Mastodon)*
* [neurodifferent.me](https://neurodifferent.me) - a friendly space for neurodifferent folks *(Mastodon)*
* [aviators.network](https://aviators.network) - for aviation enthusiasts *(Mastodon)*
* [machteburch.social](https://machteburch.social) - Magdeburg *(Mastodon)*
* [frankfurt.social](https://frankfurt.social) - Frankfurt *(Mastodon)*
* [nahe.social](https://nahe.social) - Nahe region *(Mastodon)*
* [oslo.town](https://oslo.town) - Oslo *(Mastodon)*
* [ecoevo.social](https://ecoevo.social) - for the biological ecology and evolution community *(Mastodon)*
* [veterinary.education](https://veterinary.education) - veterinary medicine *(Mastodon)*
* [bardown.space](https://bardown.space) - for hockey fans and players *(Hometown)*
* [idlethumbs.social](https://idlethumbs.social) - hosted in California *(Mastodon)*
* [okla.social](https://okla.social) - for those from or interested in Oklahoma *(Mastodon)*
* [artsculture.media](https://artsculture.media) - cultural server for artists, musicians, photographers, filmographers, poets, writers *(Mastodon)*
* [hoosier.social](https://hoosier.social) - for residents of Indiana *(Mastodon)*
* [dz.social](https://dz.social) - Algeria *(Mastodon)*
* [astronomy.city](https://astronomy.city) - for astronomy and astronomy-adjacent users *(Mastodon)*
* [paktodon.asia](https://paktodon.asia) - a Pakistani instance for the Global South *(Mastodon)*
* [baudigital.social](https://baudigital.social) - for people in architecture, civil engineering, or urban studies *(Mastodon)*

</ul>

## 🌟 Other research links
- [Pirate Parties in Fediverse](https://codeberg.org/lostinlight/distributopia/src/branch/main/caramba)
- [Mastodon server distribution](https://chaos.social/@leah/99837391793032137) - research by @Leah
