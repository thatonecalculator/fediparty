
---
layout: "post"
title: "Fediparty moves to a new home"
date: 2022-01-31
updated: 2022-01-31
tags:
    - fediverse
preview: "Fediverse.Party and its Wiki finally finished migration to a new home. Please, update your bookmarks"
url: "/en/post/fediverse.party-moved-to-a-new-home"
lang: en
authors: [{"name": "@lostinlight", "url": "https://mastodon.xyz/@lightone", "network": "mastodon"}]
---

<span class="u-goldenBg">**Fediverse.Party and its Wiki finally finished migration to a new home!**</span>

Feneas association, that has kindly hosted this website and its repository, [is being dissolved](https://codeberg.org/fediverse/fediparty/issues/46). Thank you to all Feneas members for having us (and hope to still meet you all in decentralized Web)!

Our code repository moved to [Codeberg](https://codeberg.org/fediverse/fediparty) forge. The automated migration from GitLab to Codeberg works like a charm! We're excited and honoured to be part of [Codeberg](https://social.anoxinon.de/@codeberg) community.

ActivityPub [Apps](https://codeberg.org/fediverse/delightful-fediverse-apps) and [Tools](https://codeberg.org/fediverse/delightful-activitypub-development) lists are now part of #delightful lists curated by [@humanetech](https://mastodon.social/@humanetech) (updates appear on the website, as always).

Please, update your bookmarks.

The website is now hosted by [@minoru](https://functional.cafe/@minoru). And we also have a [mirror](https://mirror.fediverse.party), just in case.



