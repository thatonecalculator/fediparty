
---
layout: "post"
title: "Fediverse turns 14 🎉"
date: 2022-05-18
updated: 2022-05-18
tags:
    - fediverse
preview: "Happy 14th Birthday, Fedi!  May your popularity and success double in the future! We prepared a fun quiz for you. Take part and share your results"
url: "/en/post/fediverse-14-years-in-2022"
lang: en
featured: true
banner: "banner.jpg"
authors: [{"name": "@lostinlight", "url": "https://mastodon.xyz/@lightone", "network": "mastodon"}, {"name": "@minoru", "url": "https://functional.cafe/@minoru", "network": "mastodon"}]
---

<span class="u-goldenBg">Happy 14th Birthday, Fedi! </span> &nbsp;Teenage years are turbulent and always full of experiments. May your current popularity and success double in the nearest future!

Fediverse grew quite a bit over the last year. To all the new users - welcome! And to all the fedizens who’ve been with us for all these years - thank you!

To add a bit of playfulness to this day, we prepared a 🌟[fun little quiz](https://14th.fediverse.party)🌟 for you. The source code will be revealed in a few days; we don’t want to make it too easy to figure out the answers. :) Please share your scores, and add **#FediQuiz**, **#Fediverse14** or **#HappyFedi2U** hashtags for visibility.

Cheers,
[Fediverse.party team](https://fediverse.party/en/about)
