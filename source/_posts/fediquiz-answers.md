
---
layout: "post"
title: "Answers to Fediquiz (spoiler alert!)"
date: 2022-06-18
updated: 2022-06-18
tags:
    - fediverse
preview: "We celebrated Fediverse's 14th birthday with a little quiz about its history and key projects. Let's look at the correct answers for all the questions!"
url: "/en/post/fediquiz-answers"
lang: en
authors: [{"name": "@minoru", "url": "https://functional.cafe/@minoru", "network": "mastodon"}, {"name": "@lostinlight", "url": "https://mastodon.xyz/@lightone", "network": "mastodon"}]
---

We celebrated Fediverse's 14th birthday with [a little quiz](https://14th.fediverse.party) about its history and key projects. A few of you asked us to publish the correct answers. Here we go!

There were three sets of questions, each having different number of answers. Furthermore, each attempt at the quiz presented you with new questions. To achieve that, we had three difficulty levels (easy, medium, hard), and we picked four questions from each. Shuffling them, we got a unique quiz every time. We hope it was fun :) Now let's get to the answers.

### Easy questions

**Which one is a video hosting platform?** PeerTube.

**Which one is an image sharing platform?** Pixelfed.

**Which one is a music sharing platform?** Funkwhale.

**Which one is a platform for events and meetups?** Mobilizon (although we shouldn't forget Gancio either).

**Which project has a small Arctic rodent as a mascot?** Lemmy. <img src="/img/misc/lemmy.svg" style="display: inline; width: 45px; vertical-align: middle;"> We spent so-o-o much time trying to describe mascots in a way that is not a dead give-away.

**Which project has a large marine mammal as a mascot?** Funkwhale. In fact, it has 3 whale mascots - blue Betty, green Harriet and pink Wanda! All drawm by a community member Robin.

![illustration of 3 whales](/en/post/fediquiz-answers/funkwhale-mascots.jpg)

**Which project has a marine mollusk as a mascot?** PeerTube. <img src="/img/mascots/peertube-1.png" style="display: inline"> The cuttlefish named Papa Sepia was created by David Revoy.

**Whose mascot looks like a cute bear and feeds on bamboo?** Pixelfed. <img src="/img/mascots/pixelfed-1.png" style="display: inline"> You can find the original assets of red panda Fred [here](https://github.com/pixelfed/brand-assets).

**Which project has a small swift mammal with elongated ears as a mascot?** Friendica. <img src="/img/mascots/friendica-1.png" style="display: inline"> The community named the mascot Flaxy O'Hare, it was drawn by lostinlight.

**Whose mascot animal is native to Australia and feeds on eucalyptus?** Hubzilla. Its mascot is red koala. <img src="/img/mascots/hubzilla-1.png" style="display: inline"> The initial creator of Hubzilla lives in Australia.

**Which project has a small nocturnal fox as a mascot?** Mobilizon. <img src="/en/post/fediquiz-answers/mobilizon.jpg" style="display: inline"> [Rose](https://framablog.org/2020/10/27/photo-novel-guided-tour-of-mobilizon/) the fennec was designed by David Revoy.

**Which project has a flowering plant as a mascot?** diaspora*. The word “diaspora” refers to the dispersal of seeds (or people) over a wide area and the asterisk in the name represents a fluffy dandelion seed.

**Which project does <i>not</i> directly contribute to Fediverse’s development?** Twitter BlueSky.

**Which project directly contributes to Fediverse’s development and success?** [Glitch-Soc](https://glitch-soc.github.io/docs) – a popular fork of Mastodon with extra features.

**Who was among the editors of ActivityPub specification?** Christine Lemmer-Webber, but don't forget the other editors too: Jessica Tallon, Erin Shepherd, Amy Guy, and Evan Prodromou. Writing standards is hard; these folks deserve a medal!

### Somewhat difficult questions

**Which project was named after a music band?** Mastodon.🤘 Actually, depending on the sources, the project [may](https://mic.com/articles/173297/what-is-mastodon-everything-to-know-about-new-social-network) or [may not](http://mashable.com/2017/04/06/mastodon-band-social-media) be named after the band, but Eugen Rochko, the creator of Mastodon network, is definitely a fan of the band. By the way, one other Fedi project has musical roots too: [Lemmy](https://join-lemmy.org/docs/en/index.html#whys-it-called-lemmy) is the name of a lead singer of Motörhead.

**Which project first allowed users to add cat ears to their avatars?** Misskey. Several people told us that some Pleroma servers used to have this feature even before Misskey, but as we couldn't find it in Pleroma's code repository, the answer is still technically correct.

**What protocol is most widely adopted in Fediverse?** ActivityPub.

**How many user accounts are there in Fediverse?** 2-7 million. In related news, [the-federation.info is under development again](https://github.com/thefederationinfo/the-federation.info/pull/284). Give a helping hand if you are into frontend webdev and/or Python!

**Who published the identi.ca post that marks the beginning of Fediverse?** Evan Prodromou.

**Which one is <i>not</i> a fork of Mastodon?** Pleroma. This may seem an idiotic question, but over the years we've seen many newcomers whose acquaintance with Fediverse starts and ends with Mastodon. They come across popular Mastodon forks and assume that all Fedi microblogging platforms are some variant of modified Mastodon. This question was a reminder that Pleroma, like many other microblogging projects, has a different, independent codebase and history.

**Which one started as a fork of Pleroma?** In our quiz, the correct answer is Mobilizon, but it was pointed out to us ([twice](https://pl.fediverse.pl/objects/afc2836b-ce54-4f09-96c2-25a5edf10037)) that it wasn't a full-blown fork — Mobilizon just borrowed some of the code. We couldn't come up with a better phrasing for the question though, so it is what it is.

**What is the unofficial Fediverse mascot suggested by community members?** Phoenix - the symbol of rebirth, renewal, progress and eternity. A legendary bird that rises from the ashes, just like federated networks fall in popularity and rise again, better and stronger. Hopefully you noticed that the birds at the end of the quiz are phoenixes too!

**Which one of these supports Gopher protocol?** [Pleroma](https://blog.soykaf.com/post/gopher-support-in-pleroma).

**What’s the name of a popular Android app for Funkwhale?**  [Otter](https://github.com/apognu/otter).

**What’s the name of a multi-platform client for Lemmy?** [Lemmur](https://github.com/LemmurOrg/lemmur).

**Which two projects are developed by Framasoft?** Mobilizon and PeerTube.

**Which Linux distribution donated €10k in 2020 to fund live-streaming in PeerTube?** Debian.

### Hard questions

**Which project <i>wasn’t</i> funded by a grant from NGI Zero (NLNet Foundation)?** WriteFreely. That's right, NLNet funded *the other five*, and we could list even more! Working on infrastructure software isn't very glamorous, so grants from NGI Zero really help move things forward.

**Which project received a $70k grant from Samsung NEXT in 2019?** [Mastodon](https://mastodon.social/@Gargron/101468776092987214).

**In what city did the ActivityPub community conference happen in 2019?** Prague.

**Which project is <i>not</i> written in Go?** Lemmy 🦀

**Which project is written in Python?** Bookwyrm 🐍

**Which project is written in Elixir?** Mobilizon 💧

**When was ActivityPub standardized?** 2018. Can you believe it's been four years already?

**How was identi.ca software called at first?** Laconica.

**Initially, GNU Social was a set of plugins for…** StatusNet.

**What was the original name of the project now known as Hubzilla?** Redmatrix.

**What was the original name of the project now known as Friendica?** Mistpark.

&nbsp;

And there you have it — all the answers to all the questions in the quiz! We hope this was both fun and educational. See you in Fediverse!
